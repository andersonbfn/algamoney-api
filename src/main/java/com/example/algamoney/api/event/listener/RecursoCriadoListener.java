/**
 * 
 */
package com.example.algamoney.api.event.listener;

import java.net.URI;

import javax.servlet.http.HttpServletResponse;

import org.springframework.context.ApplicationListener;
import org.springframework.stereotype.Component;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;

import com.example.algamoney.api.event.RecursoCriadoEvent;

/**
 * @author Anderson
 *
 */
@Component
public class RecursoCriadoListener implements ApplicationListener<RecursoCriadoEvent> {

	@Override
	public void onApplicationEvent(final RecursoCriadoEvent event) {
		final HttpServletResponse response = event.getResponse();
		final Long codigo = event.getCodigo();
		
		adicionarHeaderLocation(response, codigo);

	}

	private void adicionarHeaderLocation(final HttpServletResponse response, final Long codigo) {
		final URI uri = ServletUriComponentsBuilder.fromCurrentRequestUri().path("/{codigo}")
				.buildAndExpand(codigo).toUri();
		response.setHeader("Location", uri.toASCIIString());
	}

}
