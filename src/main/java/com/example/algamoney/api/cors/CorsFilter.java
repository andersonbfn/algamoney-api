/**
 * 
 */
package com.example.algamoney.api.cors;

import java.io.IOException;

import javax.servlet.Filter;
import javax.servlet.FilterChain;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.Ordered;
import org.springframework.core.annotation.Order;
import org.springframework.stereotype.Component;

import com.example.algamoney.api.config.property.AlgamoneyApiProperty;

/**
 * Filtro para prover "CORS" uma vez que o suporte nativo do Spring (@CrossOrigin) não "casa" bem com O'Auth 2.
 * @author Anderson
 */
@Component
@Order(Ordered.HIGHEST_PRECEDENCE)
public class CorsFilter implements Filter {

	@Autowired
	private AlgamoneyApiProperty algamoneyApiProperty;

	@Override
	public void doFilter(ServletRequest request, ServletResponse response, FilterChain chain)
			throws IOException, ServletException {
		
		final HttpServletRequest requestHttp = (HttpServletRequest) request;
		final HttpServletResponse responseHttp = (HttpServletResponse) response;
		
		responseHttp.setHeader("Access-Control-Allow-Origin", algamoneyApiProperty.getOriginPermitida());
		responseHttp.setHeader("Access-Control-Allow-Credentials", "true"); // para permitir o "Cookie" ser enviado. (cookie faz parte de "Credentials")
		
		// Se for "pre-flight" request e for da "origem permitida" ...
		if ("OPTIONS".equals(requestHttp.getMethod()) && algamoneyApiProperty.getOriginPermitida().equals(requestHttp.getHeader("Origin"))) {
			responseHttp.setHeader("Access-Control-Allow-Methods", "POST, GET, DELETE, PUT, OPTIONS"); // Metodos permitidos
			responseHttp.setHeader("Access-Control-Allow-Headers", "Authorization, Content-Type, Accept"); // Headers permitidos
			responseHttp.setHeader("Access-Control-Max-Age", "3600"); // Só depois de 1 hora que o browser vai atualizar a requisição de options...
			
			responseHttp.setStatus(HttpServletResponse.SC_OK);
		} else { // Se for request efetivo... 
			chain.doFilter(request, response); // deixa prosseguir normalmente...
		}
		
	}

}
