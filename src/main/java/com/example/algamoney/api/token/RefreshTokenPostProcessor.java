/**
 * 
 */
package com.example.algamoney.api.token;

import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.MethodParameter;
import org.springframework.http.MediaType;
import org.springframework.http.converter.HttpMessageConverter;
import org.springframework.http.server.ServerHttpRequest;
import org.springframework.http.server.ServerHttpResponse;
import org.springframework.http.server.ServletServerHttpRequest;
import org.springframework.http.server.ServletServerHttpResponse;
import org.springframework.security.oauth2.common.DefaultOAuth2AccessToken;
import org.springframework.security.oauth2.common.OAuth2AccessToken;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.servlet.mvc.method.annotation.ResponseBodyAdvice;

import com.example.algamoney.api.config.property.AlgamoneyApiProperty;

/**
 * Processador após o refresh token ter sido criado.
 * @author Anderson
 */
@ControllerAdvice
public class RefreshTokenPostProcessor implements ResponseBodyAdvice<OAuth2AccessToken> {

	@Autowired
	private AlgamoneyApiProperty algamoneyApiProperty;

	@Override
	public boolean supports(final MethodParameter returnType, final Class<? extends HttpMessageConverter<?>> converterType) {
		return returnType.getMethod().getName().equals("postAccessToken");
	}

	@Override
	public OAuth2AccessToken beforeBodyWrite(final OAuth2AccessToken body, final MethodParameter returnType,
			final MediaType selectedContentType, final Class<? extends HttpMessageConverter<?>> selectedConverterType,
			final ServerHttpRequest request, final ServerHttpResponse response) {
		
		final String refreshToken = body.getRefreshToken().getValue();
		
		final HttpServletRequest requestServletHttp = ((ServletServerHttpRequest) request).getServletRequest();
		final HttpServletResponse responseServletHttp = ((ServletServerHttpResponse) response).getServletResponse();
		
		final DefaultOAuth2AccessToken tokenAsDefault =(DefaultOAuth2AccessToken) body;
		
		adicionarRefreshTokenNoCookie(refreshToken, requestServletHttp, responseServletHttp);
		removerRefreshTokenDoBody(tokenAsDefault);
		
		return body;
	}

	private void removerRefreshTokenDoBody(DefaultOAuth2AccessToken tokenAsDefault) {
		tokenAsDefault.setRefreshToken(null);
	}

	private void adicionarRefreshTokenNoCookie(final String refreshToken, final HttpServletRequest requestServletHttp,
			final HttpServletResponse responseServletHttp) {
		final Cookie refreshTokenCookie = new Cookie("refreshToken", refreshToken);
		refreshTokenCookie.setHttpOnly(true); // Evita de ser acessado via Javascript!
		refreshTokenCookie.setSecure(algamoneyApiProperty.getSeguranca().isEnableHttps());
		refreshTokenCookie.setPath(requestServletHttp.getContextPath()+"/oauth/token");
		refreshTokenCookie.setMaxAge(2592000); // Ex: 30 dias...
		responseServletHttp.addCookie(refreshTokenCookie);
	}

}
