/**
 * 
 */
package com.example.algamoney.api.token;

import java.io.IOException;
import java.util.Map;

import javax.servlet.Filter;
import javax.servlet.FilterChain;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletRequestWrapper;

import org.apache.catalina.util.ParameterMap;
import org.springframework.core.Ordered;
import org.springframework.core.annotation.Order;
import org.springframework.stereotype.Component;

/**
 * Filtro pré processador de token para devolve-lo em requisições seguidas .
 * @author Anderson
 */
@Component
@Order(Ordered.HIGHEST_PRECEDENCE)
public class RefreshTokenCookiePreProcessorFilter implements Filter {

	@Override
	public void doFilter(final ServletRequest request, final ServletResponse response, final FilterChain chain)
			throws IOException, ServletException {

		HttpServletRequest requestHttp = (HttpServletRequest) request;
		
		if ("/oauth/token".equalsIgnoreCase(requestHttp.getRequestURI())
				&& "refresh_token".equals(requestHttp.getParameter("grant_type"))
				&& requestHttp.getCookies() != null) {
			
			for (final Cookie cookie : requestHttp.getCookies()) {
				if (cookie.getName().equals("refreshToken")) {
					final String refreshToken = cookie.getValue();
					requestHttp = new MyServletRequestWrapper(requestHttp, refreshToken);
					break;
				}
			}
			
		}
		
		// Prossegue com a requisição substituida por uma nova com o cookie copiado para requestParam...
		chain.doFilter(requestHttp, response);
	}

	static class MyServletRequestWrapper extends HttpServletRequestWrapper {

		private String refreshToken;

		public MyServletRequestWrapper(final HttpServletRequest request, final String refreshToken) {
			super(request);
			this.refreshToken = refreshToken;
		}
		
		@Override
		public Map<String, String[]> getParameterMap() {
			final ParameterMap<String, String[]> mapWrapper = new ParameterMap<String, String[]>(getRequest().getParameterMap());
			mapWrapper.put("refresh_token", new String[] { refreshToken }  );
			mapWrapper.setLocked(true);
			return mapWrapper;
		}
		
	}
}
