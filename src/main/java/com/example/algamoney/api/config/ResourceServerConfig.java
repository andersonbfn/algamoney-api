/**
 * 
 */
package com.example.algamoney.api.config;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Profile;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.http.SessionCreationPolicy;
import org.springframework.security.oauth2.config.annotation.web.configuration.EnableResourceServer;
import org.springframework.security.oauth2.config.annotation.web.configuration.ResourceServerConfigurerAdapter;
import org.springframework.security.oauth2.config.annotation.web.configurers.ResourceServerSecurityConfigurer;
import org.springframework.security.oauth2.provider.expression.OAuth2MethodSecurityExpressionHandler;

/**
 * Configuração do Servidor de Recursos (O'Auth 2)
 * @author Anderson
 */
@Profile("oauth-security")
@Configuration
@EnableWebSecurity
@EnableResourceServer
public class ResourceServerConfig extends ResourceServerConfigurerAdapter {

//	@Autowired
//	private UserDetailsService userDetailsService;
//	
//	@Autowired
//	public void configure(AuthenticationManagerBuilder auth) throws Exception {
//		auth.userDetailsService(userDetailsService)
//			.passwordEncoder(passwordEncoder());
//		
//		// Se Spring => 5, use o prefixo "{noop}" para não encodar senhas (somente pra testes)...
////		auth.inMemoryAuthentication()
////			.withUser("admin").password("{noop}admin").roles("ROLE");
//	}

	@Override
	public void configure(HttpSecurity http) throws Exception {
		http.authorizeRequests()
				.antMatchers("/categorias").permitAll()
	 			.anyRequest().authenticated()
			.and()
				// apesar do "configure" de resourceserver já providenciar, mantém para garantir...
				.sessionManagement().sessionCreationPolicy(SessionCreationPolicy.STATELESS) 
			.and()
				.csrf().disable();
	}

	@Override
	public void configure(ResourceServerSecurityConfigurer resources) throws Exception {
		resources.stateless(true);
	}

	@Bean
	public OAuth2MethodSecurityExpressionHandler createExpressionHandler() {
		return new OAuth2MethodSecurityExpressionHandler();
	}
}
