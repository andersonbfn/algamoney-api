/**
 * 
 */
package com.example.algamoney.api.model;

/**
 * @author Anderson
 */
public enum TipoLancamento {
	RECEITA, DESPESA
}
