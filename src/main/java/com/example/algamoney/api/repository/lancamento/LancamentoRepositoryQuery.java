/**
 * 
 */
package com.example.algamoney.api.repository.lancamento;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import com.example.algamoney.api.model.Lancamento;
import com.example.algamoney.api.repository.filter.LancamentoFilter;
import com.example.algamoney.api.repository.projection.ResumoLancamento;

/**
 * @author Anderson
 */
public interface LancamentoRepositoryQuery {

	Page<Lancamento> filtrar(LancamentoFilter filter, Pageable pageRequest);

	Page<ResumoLancamento> resumir(LancamentoFilter filter, Pageable pageRequest);
}
