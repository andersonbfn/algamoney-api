/**
 * 
 */
package com.example.algamoney.api.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.example.algamoney.api.model.Categoria;

/** 
 * 
 * @author anderson.bernardo
 * @since 13/12/2018 
 */
@Repository
public interface CategoriaRepository extends JpaRepository<Categoria, Long> {
}
