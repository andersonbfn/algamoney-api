/**
 * 
 */
package com.example.algamoney.api.repository.filter;

import java.time.LocalDate;

import org.springframework.format.annotation.DateTimeFormat;

/**
 * @author Anderson
 */
public class LancamentoFilter {
	
	private String descricao;
	
	@DateTimeFormat(pattern = "yyyy-MM-dd")
	private LocalDate dataVencimentoDe;
	
	@DateTimeFormat(pattern = "yyyy-MM-dd")
	private LocalDate dataVencimentoAte;
	
	public String getDescricao() {
		return descricao;
	}
	
	public void setDescricao(String descricao) {
		this.descricao = descricao;
	}

	public LocalDate getDataVencimentoDe() {
		return dataVencimentoDe;
	}

	public void setDataVencimentoDe(LocalDate dataVencimentoDe) {
		this.dataVencimentoDe = dataVencimentoDe;
	}

	public LocalDate getDataVencimentoAte() {
		return dataVencimentoAte;
	}

	public void setDataVencimentoAte(LocalDate dataVencimentoAte) {
		this.dataVencimentoAte = dataVencimentoAte;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((dataVencimentoAte == null) ? 0 : dataVencimentoAte.hashCode());
		result = prime * result + ((dataVencimentoDe == null) ? 0 : dataVencimentoDe.hashCode());
		result = prime * result + ((descricao == null) ? 0 : descricao.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		LancamentoFilter other = (LancamentoFilter) obj;
		if (dataVencimentoAte == null) {
			if (other.dataVencimentoAte != null)
				return false;
		} else if (!dataVencimentoAte.equals(other.dataVencimentoAte))
			return false;
		if (dataVencimentoDe == null) {
			if (other.dataVencimentoDe != null)
				return false;
		} else if (!dataVencimentoDe.equals(other.dataVencimentoDe))
			return false;
		if (descricao == null) {
			if (other.descricao != null)
				return false;
		} else if (!descricao.equals(other.descricao))
			return false;
		return true;
	}

	@Override
	public String toString() {
		return "LancamentoFilter [descricao=" + descricao + ", dataVencimentoDe=" + dataVencimentoDe
				+ ", dataVencimentoAte=" + dataVencimentoAte + "]";
	}
	
}
