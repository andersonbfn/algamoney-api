/**
 * 
 */
package com.example.algamoney.api.exceptionhandler;

import java.util.Arrays;
import java.util.LinkedList;
import java.util.List;

import org.apache.commons.lang3.exception.ExceptionUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.MessageSource;
import org.springframework.context.i18n.LocaleContextHolder;
import org.springframework.dao.DataIntegrityViolationException;
import org.springframework.dao.EmptyResultDataAccessException;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.http.converter.HttpMessageNotReadableException;
import org.springframework.validation.BindingResult;
import org.springframework.validation.FieldError;
import org.springframework.web.bind.MethodArgumentNotValidException;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.context.request.WebRequest;
import org.springframework.web.servlet.mvc.method.annotation.ResponseEntityExceptionHandler;

/**
 * Tratador de exceções de "ResponseEntity".
 * @author Anderson
 */
@ControllerAdvice
public class AlgamoneyExceptionHandler extends ResponseEntityExceptionHandler {

	@Autowired
	private MessageSource messageSource;
	
	@Override
	protected ResponseEntity<Object> handleHttpMessageNotReadable(HttpMessageNotReadableException ex,
			HttpHeaders headers, HttpStatus status, WebRequest request) {
		
		final String msgAoUsuario = messageSource.getMessage("mensagem.invalida", null, LocaleContextHolder.getLocale());
		final String msgAoDesenvolvedor = ex.getCause() != null ? ex.getCause().toString() : ex.toString();
		
		final List<Erro> erros = Arrays.asList(new Erro(msgAoUsuario, msgAoDesenvolvedor));
		
		return handleExceptionInternal(ex, erros, headers, HttpStatus.BAD_REQUEST, request);
	}
	
	@Override
	protected ResponseEntity<Object> handleMethodArgumentNotValid(MethodArgumentNotValidException ex,
			HttpHeaders headers, HttpStatus status, WebRequest request) {
		
		final List<Erro> erros = criarListaDeErros(ex.getBindingResult());
		
		return handleExceptionInternal(ex, erros, headers, HttpStatus.BAD_REQUEST, request);
	}
	
	@ExceptionHandler({ EmptyResultDataAccessException.class })
	public ResponseEntity<?> handleEmptyResultDataAccessException(final EmptyResultDataAccessException ex, final WebRequest request) {
		final String msgAoUsuario = messageSource.getMessage("recurso.nao-encontrado", null, LocaleContextHolder.getLocale());
		final String msgAoDesenvolvedor = ex.toString();
		final List<Erro> erros = Arrays.asList(new Erro(msgAoUsuario, msgAoDesenvolvedor));
		return handleExceptionInternal( ex, erros, new HttpHeaders(), HttpStatus.NOT_FOUND, request);
	}
	
	@ExceptionHandler({ DataIntegrityViolationException.class })
	public ResponseEntity<?> handleDataIntegrityViolationException(final DataIntegrityViolationException ex, final WebRequest request) {
		final String msgAoUsuario = messageSource.getMessage("recurso.operacao-nao-permitida", null, LocaleContextHolder.getLocale());
		final String msgAoDesenvolvedor = ExceptionUtils.getRootCauseMessage(ex);
		final List<Erro> erros = Arrays.asList(new Erro(msgAoUsuario, msgAoDesenvolvedor));
		return handleExceptionInternal( ex, erros, HttpHeaders.EMPTY, HttpStatus.BAD_REQUEST, request);
	}
	
	private List<Erro> criarListaDeErros(final BindingResult bindingResult) {
		final List<Erro> erros = new LinkedList<>();
		
		for (FieldError	fieldError : bindingResult.getFieldErrors()) {
			String msgUsuario = messageSource.getMessage(fieldError, LocaleContextHolder.getLocale());
			String msgDesenvolvedor = fieldError.toString();
			erros.add(new Erro(msgUsuario, msgDesenvolvedor));
		}

		return erros;
	}
	
	public static class Erro {
		private final String msgUsuario;
		private final String msgDesenvolvedor;
		
		public Erro(String msgUsuario, String msgDesenvolvedor) {
			super();
			this.msgUsuario = msgUsuario;
			this.msgDesenvolvedor = msgDesenvolvedor;
		}

		public String getMsgUsuario() {
			return msgUsuario;
		}

		public String getMsgDesenvolvedor() {
			return msgDesenvolvedor;
		}
		
	}
	
}
