/**
 * 
 */
package com.example.algamoney.api.service;

import java.util.Optional;

import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.EmptyResultDataAccessException;
import org.springframework.stereotype.Service;

import com.example.algamoney.api.model.Pessoa;
import com.example.algamoney.api.repository.PessoaRepository;

/** 
 * 
 * @author anderson.bernardo
 * @since 19/12/2018 
 */
@Service
public class PessoaService {

	@Autowired
	private PessoaRepository pessoaRepository;
	
	public Pessoa atualizar(final Long codigo, final Pessoa pessoa) {
		final Pessoa pessoaFound = buscarPessoaPeloCodigo(codigo);
		BeanUtils.copyProperties(pessoa, pessoaFound, "codigo");
		return pessoaRepository.save(pessoaFound);
	}
	
	public Pessoa buscarPessoaPeloCodigo(final Long codigo) {
		final Optional<Pessoa> optPessoa = pessoaRepository.findById(codigo);
		if (optPessoa.isPresent()) {
			return optPessoa.get();
		}

		throw new EmptyResultDataAccessException(1); // Será tratada no ControllerAdvice...
	}

	public Pessoa atualizarPropriedadeAtivo(Long codigo, Boolean ativo) {
		final Pessoa pessoaSalva = buscarPessoaPeloCodigo(codigo);
		pessoaSalva.setAtivo(ativo);
		return pessoaRepository.save(pessoaSalva);
	}
	
}
