/**
 * 
 */
package com.example.algamoney.api.service;

import java.util.Optional;

import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.example.algamoney.api.model.Lancamento;
import com.example.algamoney.api.model.Pessoa;
import com.example.algamoney.api.repository.LancamentoRepository;
import com.example.algamoney.api.repository.PessoaRepository;
import com.example.algamoney.api.service.exception.PessoaInexistenteOuInativaExcepton;

/**
 * @author Anderson
 */
@Service
public class LancamentoService {

	@Autowired
	private PessoaRepository pessoaRepository;
	
	@Autowired
	private LancamentoRepository lancamentoRepository;
	
	public Lancamento salvar(Lancamento lancamento) {
		final Optional<Pessoa> optPessoa = pessoaRepository.findById(lancamento.getPessoa().getCodigo());
		if (!optPessoa.isPresent() || optPessoa.get().isInativo()) {
			throw new PessoaInexistenteOuInativaExcepton();
		}
		
		return lancamentoRepository.save(lancamento);
	}
	
	public Lancamento atualizar(final Long codigo, Lancamento lancamento) {
		final Lancamento lancamentoExistente = buscaLancamentoExistente(codigo);
		if (!lancamento.getPessoa().equals(lancamentoExistente.getPessoa())) {
			validarPessoa(lancamento);
		}
		
		BeanUtils.copyProperties(lancamento, lancamentoExistente, "codigo");
		
		return lancamentoRepository.save(lancamentoExistente);
	}

	private void validarPessoa(Lancamento lancamento) {
		Optional<Pessoa> optPessoa = Optional.empty();
		if (lancamento.getPessoa().getCodigo() != null) {
			optPessoa = pessoaRepository.findById(lancamento.getPessoa().getCodigo());
		}
		
		optPessoa.filter(p -> !p.isInativo()).orElseThrow(PessoaInexistenteOuInativaExcepton::new);
	}

	private Lancamento buscaLancamentoExistente(Long codigo) {
		final Optional<Lancamento> lancamentoFound = lancamentoRepository.findById(codigo);
		lancamentoFound.orElseThrow(IllegalArgumentException::new);
		return lancamentoFound.get();
	}

}
