/**
 * 
 */
package com.example.algamoney.api.resource;

import java.util.List;
import java.util.Optional;

import javax.servlet.http.HttpServletResponse;
import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationEventPublisher;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.example.algamoney.api.event.RecursoCriadoEvent;
import com.example.algamoney.api.model.Categoria;
import com.example.algamoney.api.repository.CategoriaRepository;

/** 
 * Controllador REST de dados de "categoria".
 * @author anderson.bernardo
 * @since 13/12/2018 
 */
@RestController
@RequestMapping("/categorias")
public class CategoriaResource {

	@Autowired
	private CategoriaRepository categoriaRepository;
	
	@Autowired
	private ApplicationEventPublisher publisher;

//	@CrossOrigin(maxAge = 10) // TODO Verificar se a integração do CORS do spring com O'Auth2 já está consistente atualmente.
	@GetMapping
	@PreAuthorize("hasAuthority('ROLE_PESQUISAR_CATEGORIA') and #oauth2.hasScope('read')")
	public List<Categoria> listar() {
		return categoriaRepository.findAll();
	}
	
	@PostMapping
//	@ResponseStatus(HttpStatus.CREATED) // qdo usado "responseentity" com "created" não necessita mais da annotation
	@PreAuthorize("hasAuthority('ROLE_CADASTRAR_CATEGORIA') and #oauth2.hasScope('write')") 
	public ResponseEntity<Categoria> criar(@Valid @RequestBody final Categoria entity, final HttpServletResponse response) {
		final Categoria saved = categoriaRepository.save(entity);
		
		publisher.publishEvent(new RecursoCriadoEvent(this, response, saved.getCodigo()));

		return ResponseEntity.status(HttpStatus.CREATED).body(saved);
	}
	
	@GetMapping("/{codigo}")
	@PreAuthorize("hasAuthority('ROLE_PESQUISAR_CATEGORIA') and #oauth2.hasScope('read')")
	public ResponseEntity<Categoria> buscarPeloCodigo(@PathVariable final Long codigo) {
		final Optional<Categoria> optResult = categoriaRepository.findById(codigo);
		return optResult.isPresent() ? ResponseEntity.ok(optResult.get()) : ResponseEntity.notFound().build();
	}
	
}
