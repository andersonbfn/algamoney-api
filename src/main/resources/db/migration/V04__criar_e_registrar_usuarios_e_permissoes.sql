CREATE TABLE test.usuario (
	codigo serial8 NOT NULL,
	nome varchar(50) NOT NULL,
	email varchar(50) NOT NULL,
	senha varchar(150) NOT NULL,
	CONSTRAINT usuario_pk PRIMARY KEY (codigo)
);

CREATE TABLE test.permissao (
	codigo serial8 NOT NULL,
	descricao varchar(50) NOT NULL,
	CONSTRAINT permissao_pk PRIMARY KEY (codigo)
);

CREATE TABLE test.usuario_permissao (
	codigo_usuario int8 NOT NULL,
	codigo_permissao int8 NOT NULL,
	CONSTRAINT usuario_permissao_pk PRIMARY KEY (codigo_usuario,codigo_permissao),
	CONSTRAINT usuario_permissao_usuario_fk FOREIGN KEY (codigo_usuario) REFERENCES test.usuario(codigo),
	CONSTRAINT usuario_permissao_permissao_fk FOREIGN KEY (codigo_permissao) REFERENCES test.permissao(codigo)
);

INSERT INTO test.usuario (codigo, nome, email, senha) values (1, 'Administrador', 'admin@algamoney.com', '$2a$10$X607ZPhQ4EgGNaYKt3n4SONjIv9zc.VMWdEuhCuba7oLAL5IvcL5.');
INSERT INTO test.usuario (codigo, nome, email, senha) values (2, 'Maria Silva', 'maria@algamoney.com', '$2a$10$Zc3w6HyuPOPXamaMhh.PQOXvDnEsadztbfi6/RyZWJDzimE8WQjaq');

INSERT INTO test.permissao (codigo, descricao) values (1, 'ROLE_CADASTRAR_CATEGORIA');
INSERT INTO test.permissao (codigo, descricao) values (2, 'ROLE_PESQUISAR_CATEGORIA');

INSERT INTO test.permissao (codigo, descricao) values (3, 'ROLE_CADASTRAR_PESSOA');
INSERT INTO test.permissao (codigo, descricao) values (4, 'ROLE_REMOVER_PESSOA');
INSERT INTO test.permissao (codigo, descricao) values (5, 'ROLE_PESQUISAR_PESSOA');

INSERT INTO test.permissao (codigo, descricao) values (6, 'ROLE_CADASTRAR_LANCAMENTO');
INSERT INTO test.permissao (codigo, descricao) values (7, 'ROLE_REMOVER_LANCAMENTO');
INSERT INTO test.permissao (codigo, descricao) values (8, 'ROLE_PESQUISAR_LANCAMENTO');

-- admin
INSERT INTO test.usuario_permissao (codigo_usuario, codigo_permissao) values (1, 1);
INSERT INTO test.usuario_permissao (codigo_usuario, codigo_permissao) values (1, 2);
INSERT INTO test.usuario_permissao (codigo_usuario, codigo_permissao) values (1, 3);
INSERT INTO test.usuario_permissao (codigo_usuario, codigo_permissao) values (1, 4);
INSERT INTO test.usuario_permissao (codigo_usuario, codigo_permissao) values (1, 5);
INSERT INTO test.usuario_permissao (codigo_usuario, codigo_permissao) values (1, 6);
INSERT INTO test.usuario_permissao (codigo_usuario, codigo_permissao) values (1, 7);
INSERT INTO test.usuario_permissao (codigo_usuario, codigo_permissao) values (1, 8);

-- maria
INSERT INTO test.usuario_permissao (codigo_usuario, codigo_permissao) values (2, 2);
INSERT INTO test.usuario_permissao (codigo_usuario, codigo_permissao) values (2, 5);
INSERT INTO test.usuario_permissao (codigo_usuario, codigo_permissao) values (2, 8);