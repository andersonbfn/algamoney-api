CREATE TABLE test.categoria (
	codigo serial8 primary KEY,
	nome varchar(50) NOT NULL
);

INSERT INTO test.categoria (nome) values ('Lazer');
INSERT INTO test.categoria (nome) values ('Alimentação');
INSERT INTO test.categoria (nome) values ('Supermercado');
INSERT INTO test.categoria (nome) values ('Farmácia');
INSERT INTO test.categoria (nome) values ('Outros');